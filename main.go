package p

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"
	"strconv"
)

type Message struct {
	Object_kind      string            `json:"object_kind"`
	User             *User             `json:"user"`
	Project          *Project          `json:"project"`
	ObjectAttributes *ObjectAttributes `json:"object_attributes"`
}
type User struct {
	Name     string `json:"name"`
	Username string `json:"username"`
}
type Project struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}
type ObjectAttributes struct {
	ID    int    `json:"iid"`
	Title string `json:"title"`
}

func GitBotGCE(w http.ResponseWriter, r *http.Request) {

	var d Message
	baseurl := "https://gitlab.com/api/v4"
	note := "Thanks%20for%20creating%20a%20new%20issue."
	apitoken := os.Getenv("GITLABTOKEN")

	if err := json.NewDecoder(r.Body).Decode(&d); err != nil {
		fmt.Fprint(w, "Decoding error")
		return
	}

	url := baseurl + "/projects/" + strconv.Itoa(d.Project.ID) + "/issues/" + strconv.Itoa(d.ObjectAttributes.ID) + "/notes?body=" + note

	req, err := http.NewRequest("POST", url, nil)
	req.Header.Set("Private-Token", apitoken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	fmt.Fprint(w, resp.Status)

}
